package com.example.department.exception;

import lombok.Getter;
import org.springframework.http.HttpStatus;
@Getter
public class ResourceNotFoundException extends RuntimeException {
    private final HttpStatus httpStatus;

    public ResourceNotFoundException(HttpStatus httpStatus, String message) {
        super(message);
        this.httpStatus = httpStatus;
    }
}
