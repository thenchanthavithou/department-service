package com.example.department.controller;

import com.example.department.dto.DepartmentDTO;
import com.example.department.service.impl.DepartmentServiceImpl;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/departments")
@AllArgsConstructor
public class DepartmentController {
    private final DepartmentServiceImpl departmentService;
    @PostMapping
    public ResponseEntity<DepartmentDTO> saveDepartment(
            @RequestBody DepartmentDTO departmentDTO
    ) {
        DepartmentDTO response = departmentService.save(departmentDTO);
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @GetMapping("/{department-code}")
    public ResponseEntity<DepartmentDTO> getDepartment(
            @PathVariable("department-code") String departmentCode
    ) {
        DepartmentDTO response = departmentService.getById(departmentCode);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
