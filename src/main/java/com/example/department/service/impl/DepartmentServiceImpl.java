package com.example.department.service.impl;

import com.example.department.dto.DepartmentDTO;
import com.example.department.exception.ResourceNotFoundException;
import com.example.department.model.Department;
import com.example.department.repository.DepartmentRepository;
import com.example.department.service.AbstractDepartmentService;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class DepartmentServiceImpl extends AbstractDepartmentService<Department, DepartmentDTO, String> {
    private final DepartmentRepository departmentRepository;
    private final ModelMapper mapper;

    @Override
    public DepartmentDTO save(DepartmentDTO departmentDTO) {
        Department department = mapper.map(departmentDTO, Department.class);
        return mapper.map(departmentRepository.save(department), DepartmentDTO.class);
    }

    @Override
    public DepartmentDTO getById(String code) {
        Department department = departmentRepository.findByDepartmentCode(code)
                .orElseThrow(() -> new ResourceNotFoundException(
                        HttpStatus.BAD_REQUEST ,"Could not find department with code " + code));
        return mapper.map(department, DepartmentDTO.class);
    }
}
