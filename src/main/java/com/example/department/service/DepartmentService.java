package com.example.department.service;


public interface DepartmentService<D, T> {
    D save(D departmentDTO);
    D getById(T code);
}
